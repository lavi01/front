const colors = require('tailwindcss/colors')
module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'poppins': ['Poppins', 'sans-serif'],
      'domine': ['Domine', 'serif']
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      newgray: {
        DEFAULT: '#AEBFBD',
        light: '#CDCDCD' 
      },
      brown: {
        DEFAULT: '#BE6A51',
        light: '#CCC5BD'
      }
    },
    container: {
      screens: {
        lg: '960px',
        xl: '1140px',
        '2xl': '1140px'
      }
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
