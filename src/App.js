import React, { useState, useEffect } from 'react';
import axios from "axios";
import './styles/App.css'; 
import logo from './app-logo.svg';
import logo_white from './images/logo-white.svg';
import Menu from './components/Menu';
import Pagination from './components/Pagination';
import Footer from './components/Footer';
import Page1 from "./components/Page1";
import Page2 from "./components/Page2";

const baseURL="https://craft-blog.herokuapp.com/web/api";
// const baseURL="http://localhost:8085/api";
const token = '42t42yQns-pEuxIBnuBhyIJK24I6K849';
const headers = {
    Authorization: `Bearer ${token}`,
    'content-type': 'application/graphql'
};

function App() {
    const [currentPage, setCurrentPage] = useState(1);
    const [popularPost, setPopularPost] = useState(null);
    const [featuredArticles, setFeaturedArticles] = useState([]);
    const [articles, setArticles] = useState([]);
    // const [pages, setPages] = useState(1);
    const pages = 2;

    const changePage = (newPage) => {
        setCurrentPage(newPage);
    }

    const  getPopularArticle = () => {
        axios.get(baseURL, {
            headers,            
            params: {
                query: `{
                entries(section:"articles", limit: 1, orderBy: "views DESC", featured: false){
                    id
                    dateCreated @formatDateTime (format: "M. d. Y")
                    title   
                    author{
                      name
                      fullName
                      friendlyName
                      photo{
                        url
                        width
                        height
                      }
                    }
                    ... on articles_articles_Entry {
                      excerpt
                        likes
                      views
                      featured 
                      image{
                        id
                        url
                        width
                        height
                      }
                      tag {
                        id
                        title
                      }
                    }
                  }
                }
                `
            }
        }).then((result)=>{
            const post = (typeof result.data.data.entries[0] !== "undefined") ? result.data.data.entries[0] : null;
            setPopularPost(post);            
        }).catch(error => console.log(error));;
    }

    const geFeaturedArticles = () => {        
        axios.get(baseURL, {
            headers,            
            params: {
                query: `{
                    entries(section:"articles", limit: 6, orderBy: "id ASC", featured: true){
                      id
                      dateCreated @formatDateTime (format: "M. d. Y")
                      title   
                      author{
                        name
                        fullName
                        friendlyName
                        photo{
                          url
                          width
                          height
                        }
                      }
                      ... on articles_articles_Entry {
                        excerpt
                          likes
                        views
                        featured 
                        image{
                          id
                          url
                          width
                          height
                        }
                        tag {
                          id
                          title
                        }
                      }
                    }
                }`
            }
        }).then((result)=>{
            const posts = (typeof result.data.data.entries !== "undefined" && result.data.data.entries.length) ? result.data.data.entries : [];
            setFeaturedArticles(posts);            
        }).catch(error => console.log(error));;
    }

    const getArticles = () => {
        let ids = [];
        let variables = {};

        if(popularPost) {
            ids.push(popularPost.id);
        }
        if(featuredArticles) {
            featuredArticles.map((item) => {
                ids.push(item.id)
                return true
            })
        }
        
        if(ids.length) {
            const ids_final = ["not", ...ids];
            variables = {
                "section": "articles",
                "id": ids_final
            }
        } else {
            variables = {
                "section": "articles"
            }
        }

        axios.get(baseURL, {
            headers,            
            params: {
                query: `
                query($section: [String], $id: [QueryArgument!]) {
                    entries(section:$section, id: $id, limit: 3, offset: 0, orderBy: "dateCreated DESC"){
                      id
                      dateCreated @formatDateTime (format: "M. d. Y")
                      title   
                      author{
                        name
                        fullName
                        friendlyName
                        photo{
                          url
                          width
                          height
                        }
                      }
                      ... on articles_articles_Entry {
                        excerpt
                          likes
                        views
                        featured 
                        image{
                          id
                          url
                          width
                          height
                          mediumImage: url @transform (handle: "postsList", immediately: true) 
                        }
                        tag {
                          id
                          title
                        }
                      }
                    }
                }`,
                variables: variables
            }
        }).then((result)=>{
            const posts = (typeof result.data.data.entries !== "undefined" && result.data.data.entries.length) ? result.data.data.entries : [];
            setArticles(posts);
            // getTotal();
        }).catch(error => console.log(error));
    }

    useEffect(getPopularArticle, []);
    useEffect(geFeaturedArticles, []);
    useEffect(getArticles, [featuredArticles, popularPost]);
    
    return (
    <>
        <div className="container mx-auto font-poppins">
            <Menu logo={logo} />

            <div className="px-6 pt-6 xl:p-8 xl:my-8 xl:px-16">
                { currentPage === 1 ? <Page1 popularPost={popularPost} featuredArticles={featuredArticles} articles={articles} /> : <Page2 articles={articles} />}
                {/* Pagination */}
                <Pagination pages={pages} current_page={currentPage} onChangepage={changePage} />
            </div>
        </div>

        <Footer logo_white={logo_white} />
    </>
  );
}

export default App;
