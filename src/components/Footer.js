import '../styles/footer.css';

function Footer(props){
    const logo_white = props.logo_white;
    return(
        // <footer className="page-footer xl:p-8 xl:my-8 xl:px-16">
        <footer className="page-footer">
            <div className="container mx-auto">
                <div className="xl:my-8 xl:px-16">
                    <div className="footer-content lg:grid lg:grid-cols-4 xl:grid-cols-12">
                        <div className="mb-8 lg:col xl:col-start-1 xl:col-end-5">
                            <h2>No matter what stage you're in, we can support you.</h2>
                            <ul>
                                <li><a href="./">Questions on how we can?</a></li>
                                <li><a href="./" className="font-medium">Contact Us</a></li>
                            </ul>
                        </div>
                        <div className="mb-8 lg:col xl:col-start-7 xl:col-span-2">
                            <h4>Support</h4>
                            <ul>
                                <li><a href="./">Courses</a></li>
                                <li><a href="./">Coaching</a></li>
                                <li><a href="./">Community</a></li>
                                <li><a href="./">Workshops</a></li>
                            </ul>
                        </div>
                        <div className="mb-8 lg:col xl:col-span-2">
                            <h4>Explore</h4>
                            <ul>
                                <li><a href="./">Join our Team</a></li>
                                <li><a href="./">Get Certified</a></li>
                            </ul>
                        </div>
                        <div className="mb-8 lg:col xl:col-span-2">
                            <h4>Partners</h4>
                            <ul>
                                <li><a href="./">Lorem</a></li>
                                <li><a href="./">Lorem</a></li>
                                <li><a href="./">Lorem</a></li>
                                <li><a href="./">Lorem</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="footer-bottom xl:grid xl:grid-cols-12">
                        <div className="logo xl:col-span-4">
                            <img src={logo_white} alt="Site Logo" /> 
                        </div>
                        <div className="copy xl:col-span-8">
                            <ul>
                                <li>© 2021 | All Rights Reserved</li>
                                <li><a href="./">Privacy Policy</a></li>
                                <li><a href="./">Terms of Service</a></li>
                            </ul>
                        </div>
                    </div>
                </div>                
            </div>            
        </footer>
    )
}
export default Footer;