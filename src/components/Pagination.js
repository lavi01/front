import '../styles/pagination.css';

function Pagination(props) { 
    const current_page = props.current_page; 
    const pages = props.pages;
    let links = [];
    for(var i=1; i <= pages; i++) {
        const newpage = i;
        const active_class = (i === current_page) ? "active" : "";
        const item_child = (i===current_page) ? <span>{i}</span> : <a href="./" onClick={(e)=>{e.preventDefault(); props.onChangepage(newpage)}}>{i}</a>;
        const item = <li key={i+1} className={active_class}>{item_child}</li>;
        links.push(item);
    }

    return(
        <nav className="pagination">
            <ul>
                <li className="prev"><a href="./">Previous</a></li>
                {links}
                <li className="next"><a href="./">Next</a></li>
            </ul>
        </nav>
    )
}
export default Pagination;