function Burger() {
    return(
        <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M6.25 25H43.75" stroke="#3C3B39" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M6.25 12.5H43.75" stroke="#3C3B39" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M6.25 37.5H43.75" stroke="#3C3B39" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round"/>
        </svg>
    )
}

export default Burger;