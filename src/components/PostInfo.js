import '../styles/post-info.css';

function PostInfo(props) {
    const data = props.data;
    return(
        <div className="post-info">
            <span className="post-likes">{data.likes}</span>
            <span className="post-read">{data.views}</span>
        </div>
    )
}
export default PostInfo;