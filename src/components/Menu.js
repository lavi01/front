import Burger from './icons/burger';
import React, {useState} from 'react';
import '../styles/menu.css';

function Menu(props) {
    const logo = props.logo;
    const items = [
        {value: 'Courses', active: true, login: false},
        {value: 'Coaching', active: false, login: false},
        {value: 'Community', active: false, login: false},
        {value: 'Workshops', active: false, login: false},
        {value: 'Login', active: false, login: true},
    ];
    const menuItems = items.map((item, i) => {
        return <li key={i}><a href="./" className={`${item.active ? "active-menu" : item.login ? "login-btn" : ""}`}>{item.value}</a></li>
    });
    const [openMenu, setOpenMenu] = useState(false);

    return(
        <nav className="main-menu p-6 xl:p-8">
            <div className="content flex justify-between items-center xl:gap-5">
                <a href="./"><img src={logo} alt="Logo" className="menu-logo" /></a>
                <div className="menu-items hidden xl:block flex-1 text-right">
                    <ul>{menuItems}</ul>
                </div>
                <button className="xl:hidden" onClick={(e) => {setOpenMenu(!openMenu)}}><Burger/></button>                
                <button className="btn-join">Join for Free</button>
            </div>
            <div className={`menu-items mobile xl:hidden ${openMenu ? "block" : "hidden"}`}>
                <ul>{menuItems}</ul>                
            </div>
        </nav>
    )
}

export default Menu;