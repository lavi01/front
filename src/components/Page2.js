import PostSingle from './PostSingle';

function Page2(props){
    const articlesItems = props.articles.map((item, i) => {
        return <PostSingle data={item} key={i} />
    })

    return(
        <>
            <div className="posts mt-10">
                {articlesItems}
            </div>
        </>
    )    
}
export default Page2;