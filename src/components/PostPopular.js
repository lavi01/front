import '../styles/post.css';
import '../styles/post-popular.css';
import PostInfo from './PostInfo';

function PostPopular(props) {
	const data = props.data;
    const tag = (typeof data.tag[0] !== "undefined") ? data.tag[0].title : null;
    const author = (typeof data.author !== "undefined") ? data.author : null;
    const image = data.image[0].url;
    
	return(
		<div className="post popular">
			<div className="col xl:col-span-5">				
				<div className="post-image">
                    { tag ? <div className="tag"><small>{tag}</small></div> : null}					
					<img src={image} alt="Popular post" />
					<PostInfo data={data} />
				</div>
			</div>
			<div className="col xl:col-span-7 xl:place-self-stretch">
				<h2>{data.title}</h2>
				<p>{data.excerpt}</p>
				<div className="post-author">
					<div className="author-image">
						<img src={author.photo.url} alt={author.friendlyName} />
					</div>
					<h5>{author.friendlyName}</h5>
				</div>
			</div>
		</div>
	)
}
export default PostPopular;