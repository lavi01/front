import Post from './Post';
import PostPopular from './PostPopular';
import PostSingle from './PostSingle';

function Page1(props){
    const articlesItems = props.articles.map((item, i) => {
        return <PostSingle data={item} key={i} />
    })

    return(
        <>
            {/* Featured */}
            { props.featuredArticles && props.featuredArticles.length === 6 ? 
            <div className="blog-featured ">
                <div className="grid grid-cols-1 xl:grid-cols-2 xl:gap-5">
                    <div>
                        <Post data={props.featuredArticles[0]} type="wide" />
                        <div className="grid-cols-2 gap-4 hidden xl:grid">
                            <Post data={props.featuredArticles[1]} type="vertical" />
                            <Post data={props.featuredArticles[2]} type="vertical" />
                        </div>
                    </div>
                    <div>
                        <div className="grid grid-cols-2 gap-4">
                            <Post data={props.featuredArticles[3]} type="vertical" />
                            <Post data={props.featuredArticles[4]} type="vertical" />
                        </div>
                        <Post data={props.featuredArticles[5]} type="wide" customClass="hidden xl:flex xl:items-end"  />
                    </div>
                </div>
            </div>
            : null }

            {/* Popular */}
            { props.popularPost ? 
            <div className="popular-post p-6 xl:p-10">
                <PostPopular data={props.popularPost} /> 
            </div>
            : null } 

            {/* Post grid */}
            { articlesItems ? 
            <div className="posts mt-10">
                {articlesItems}
            </div>
            : null}
        </>
    )    
}
export default Page1;