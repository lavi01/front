import '../styles/post.css';

function Post(props) {
    const post_class = (props.type) ? props.type : "";
    const custom_class = (props.customClass) ? props.customClass : "";
    
    const data = props.data;
    const tag = (typeof data.tag[0] !== "undefined") ? data.tag[0].title : null;
    const image = data.image[0].url;

    let styles = {};

    if(image) {
        styles = {
            backgroundImage: `url(${image})`
        }
    }

    return(
        <div className={`post ${post_class} ${custom_class}`} style={styles}>
            {tag ? <div className="tag"><small>{tag}</small></div> : null}
            <div className="bottom">
                <div className="title"><h2>{data.title}</h2></div>
                <div className="date">{data.dateCreated}</div>
            </div>
        </div>
    )
}

export default Post;