import '../styles/post.css';
import '../styles/post-single.css';
import PostInfo from './PostInfo';

function PostSingle(props) {
    const data = props.data;
    const image = props.data.image[0].mediumImage;
    const author = (typeof data.author !== "undefined") ? data.author : null;

    return (
        <div className="post post-single">
            <img src={image} alt={data.title} className="post-image" />
            <div className="post-author">
                <h5>{author.friendlyName}</h5>
            </div>
            <h2>{data.title}</h2>
            <PostInfo data={data} />
        </div>        
    )
}
export default PostSingle;